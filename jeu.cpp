#include "Animal.h"
#include "console.h"
#include "Entite.h"
#include "fonctions.h"
#include "main.h"
#include "Montagne.h"
#include "player.h"
#include "Terrain.h"
#include "jeu.h"

using namespace std;

bool caseInput(string& data)
{
    cin >> data;
    if (data.size() == 2)
    {
        if( (data.at(0) == 'a') || (data.at(0) == 'A')
            || (data.at(0) == 'b') || (data.at(0) == 'B')
            || (data.at(0) == 'c') || (data.at(0) == 'C')
            || (data.at(0) == 'd') || (data.at(0) == 'D')
            || (data.at(0) == 'e') || (data.at(0) == 'E') )
        {
            if ( (int(data.at(1))-48 == 1) || (int(data.at(1))-48 == 2) || (int(data.at(1))-48 == 3) || (int(data.at(1))-48 == 4) || (int(data.at(1))-48 == 5) ) //48 car 0 est pos 48 dans ASCII
            {
                return true;
            }
            else return false;
        }
        else return false;
    }
    else return false;
}

void caseTreatement(string& data, int& x, int& y)
{
    y = data.at(1);
    y = y - 49;
    if( (data.at(0) == 'a') || (data.at(0) == 'A')) x = 1;
    if( (data.at(0) == 'b') || (data.at(0) == 'B')) x = 2;
    if( (data.at(0) == 'c') || (data.at(0) == 'C')) x = 3;
    if( (data.at(0) == 'd') || (data.at(0) == 'D')) x = 4;
    if( (data.at(0) == 'e') || (data.at(0) == 'E')) x = 5;
    x--;


}

void orientationPiece(Player& p, Terrain& terrain, Console* pConsole)
{
    string data = " ";
    int x,y;
    bool correct = false;
    bool correct2 = false;
    char orient;


    system("cls");
    terrain.afficher(0,0,pConsole);
    pConsole->gotoLigCol(18,2);
    cout << endl;

    cout << "Veuillez entrer la case du pion dont vous voulez changer l'orintation (lettre puis chiffre)" << endl;
    while(!correct) correct = caseInput(data);
    caseTreatement(data,x ,y);
    cout << "Quelle orientation voulez vous lui donner ? (h, b, d, g)" << endl;
    while(!correct2) correct2 = choixOrientation(orient);

    auto plateau = terrain.getPlateau();
    if(plateau[x][y]!=nullptr)
    {
        if(plateau[x][y]->getSymbole() == p.getPiece().at(0)) plateau[x][y]->setOrientation(orient);
        else cout << "Vous n'avez pas le droit de changer l'orientation d'une piece qui n'est pas la votre" << endl<<"Vous venez de gacher un tour :)"<<std::endl;system("pause");
    }
    else std::cout<<"Vous ne pouvez pas changer l'orientation d'une case vide voyons !"<<std::endl<<"Vous venez de gacher un tour :)"<<std::endl;system("pause");

}


bool choixOrientation(char& orient)
{
    cin >> orient;
    if ( (orient == 'g') || (orient == 'd') || (orient == 'h') || (orient == 'b') || (orient == 'G') || (orient == 'D') || (orient == 'H') || (orient == 'B')) return true;
    else return false;
}
bool caseEstAdjacente(int x0,int y0,int x1,int y1)
{
    return ((x1==x0+1) ^ (x1==x0-1) ^ (y1==y0+1) ^ (y1==y0-1));
}

void ajoutPion(Player& p, Terrain& terrain, Console* pConsole, Player& p2, SAMPLE* son[SON_NB])
{
    vector<Entite*> dehors;
    string data = " ";
    bool correct = false;
    bool correct2 = false;
    int x, y;
    char orient;

    system("cls");
    terrain.afficher(0,0,pConsole);
    pConsole->gotoLigCol(18,0);
    cout << endl;

    if( p.getPiece() == "Elephant") dehors = terrain.getDehorsElephant();
    if( p.getPiece() == "Rhinoceros") dehors = terrain.getDehorsRhino();

    if(dehors.size() != 0)
    {
        cout << "Veuillez entrer la case ou vous voulez ajouter votre pion (lettre puis chiffre)" << endl;
        while(!correct) correct = caseInput(data);
        caseTreatement(data,x ,y);
        cout << "Avec quelle orientation voulez vous le faire rentrer ? (h, b, d, g)" << endl;
        while(!correct2) correct2 = choixOrientation(orient);
        if( p.getPiece() == "Elephant") terrain.faire_entrer_animal('E',orient,x,y,p,p2,son);
        if( p.getPiece() == "Rhinoceros") terrain.faire_entrer_animal('R',orient,x,y,p,p2,son);
    }
    else
    {
        cout << "Vous n'avez aucune piece a rajouter sur le terrain" << endl;
    }
}
void deplacerPion(Player& p, Terrain & terrain, Console* pConsole,Player &p2, SAMPLE* son[SON_NB])
{
    ///initialisation de variables
    int x0=0;
    int y0=0;
    int x1=0;
    int y1=0;
    char orientation;
    string data;
    string data2;
    bool correct=false;
    bool caseVide=true;
    bool correct2=false;
    bool correct3=false;
    bool caseAdja=false;

    ///Demande des donn�es
    //je dois gotolicol avant de cout
    system("cls");
    terrain.afficher(0,0,pConsole);
    pConsole->gotoLigCol(18,0);

    while(caseVide)
    {
        std::cout<<"Entrez la case du pion que vous voulez deplacer (lettre puis chiffre)"<<std::endl;
        while(!correct) correct=caseInput(data);
        caseTreatement(data,x0,y0);
        if(terrain.getPlateau()[x0][y0]==nullptr)
        {
            caseVide=true;
            correct=false;
        }
        else
        {
            caseVide=false;
            correct=true;
        }
        if(caseVide) std::cout<<"La case que vous voulez selectionner est vide... "<<std::endl;
    }

    ///Si le pion que le joueur veut jouer est jouable par lui
    if(terrain.getPlateau()[x0][y0]->getSymbole() == p.getPiece().at(0))
    {
        ///saisi de la case destination
        //+blindage de si la case est adjacente
        do
        {
            std::cout<<"Entrez la case ou vous voulez aller"<<std::endl;
            while(!correct2) correct2=caseInput(data);
            caseTreatement(data,x1,y1);
            caseAdja=caseEstAdjacente(x0,y0,x1,y1);
            if(!caseAdja )
            {
                std::cout<<"Erreur ! Vous ne pouvez que vous deplacer d'une case adjacente! "<<std::endl;
                correct2=false;
            }
        }while(!caseAdja );

        ///si la case destination est vide
        if(terrain.getPlateau()[x1][y1]==nullptr)
        {//alors on utilise deplacer sur case vide
            //saisie de l'orientation
            std::cout<<"Avec quelle orientation voulez vous le deplacer ? (h,b,d,g)"<<std::endl;
            while(!correct3) correct3 = choixOrientation(orientation);
            terrain.deplacer_sur_case_vide(x0,y0,x1,y1,orientation);
        }
        ///Sinon on utilise la fonction pousser
        else
        {
            std::cout<<"pousse pousse"<<std::endl;
//            system("pause");

            switch(terrain.getPlateau()[x0][y0]->getOrientation())
                {
                case 'h':   std::cout<<"x0="<<x0<<std::endl;
                            std::cout<<"y0="<<y0<<std::endl;
                            if (x1==x0-1) terrain.deplacer_en_poussant(y0,x0,'h',p,p2, son);
                            else std::cout<<"Vous ne pouvez pousser qu'en avant"<<std::endl;
                            break;
                case 'b': if (x1==x0+1) terrain.deplacer_en_poussant(y0,x0,'b',p,p2, son);
                            else std::cout<<"Vous ne pouvez pousser qu'en avant"<<std::endl;
                            break;
                case 'g': if (y1==y0-1) terrain.deplacer_en_poussant(y0,x0,'g',p,p2, son);
                            else std::cout<<"Vous ne pouvez pousser qu'en avant"<<std::endl;
                            break;
                case 'd':   std::cout<<"x0="<<x0<<std::endl;
                            std::cout<<"y0="<<y0<<std::endl;
                            if (y1==y0+1) terrain.deplacer_en_poussant(y0,x0,'d',p,p2, son);
                            else std::cout<<"Vous ne pouvez pousser qu'en avant"<<std::endl;
                            break;
                }
//            system("pause");

        }
    }
    else{
        std::cout<<"Vous ne pouvez pas deplacer un";
        if(terrain.getPlateau()[x0][y0]->getSymbole()=='E') std::cout<<" Elephant car vous controlez les Rhinoceros"<<std::endl;
        else std::cout<<" Rhinoceros car vous controlez les Elephants"<<std::endl;
        std::cout<<"Vous venez de gacher un precieux tour :/"<<std::endl;
       system("pause");
    }

}


void sortiePion(Player& p, Terrain & terrain, Console* pConsole)
{
    vector<Entite*> dehors;
    string data = " ";
    bool correct = false;
    int x, y;

    system("cls");
    terrain.afficher(0,0,pConsole);
    pConsole->gotoLigCol(18,2);
    cout << endl;

    if( p.getPiece() == "Elephant") dehors = terrain.getDehorsElephant();
    if( p.getPiece() == "Rhinoceros") dehors = terrain.getDehorsRhino();
    if(terrain.getPlateau().size() != 0)
    {
        cout << "Veuillez entrer la case dont vous voulez sortir le pion (lettre puis chiffre)" << endl;
        while(!correct) correct = caseInput(data);
        caseTreatement(data,x ,y);
        if( p.getPiece() == "Elephant") terrain.faire_sortir_animal(p,x,y);
        if( p.getPiece() == "Rhinoceros") terrain.faire_sortir_animal(p,x,y);
    }
    else
    {
        cout << "Vous ne pouvez pas sortir cet animal car elle n'est pas en bordure" << endl;
        system("pause");
    }
}
