#ifndef ENTITE_H
#define ENTITE_H
#include "main.h"

class Entite
{
    protected :
        char m_symbole = ' ';
        char m_orientation=' ';
        float m_poids;
        std::string m_nom;

    public :
        Entite();
        Entite( float poids);
        //Entite() surcharge
        virtual ~Entite();
        //Getters
        virtual std::string getNom() {return m_nom;}; //virtuelle

        char getSymbole() const;
        char getOrientation() const;
        float getPoids() const;
        //Setters

        void setSymbole(char _symbole);
        void setOrientation(char _orientation);
        void setPoids(float _poids);
};


#endif // ENTITE_H
