#include "main.h"
#include "player.h"
#include "Terrain.h"


void gestion_goto(int& lig, int& col, Console* pConsole);
void getconsole_size(int* POS_ECRAN_X, int* POS_ECRAN_Y);
void creationPerso(Player& p1, Player& p2);
void menu(Player& p, Console* pConsole, Terrain& terrain,Player & p2,SAMPLE* son[SON_NB]);
void deallocalisation(Terrain& terrain);

void afficher(Terrain& terrain, Console* pConsole, BITMAP* img[BMP_NB]);

void menuMode(Player& p, Console* pConsole, Terrain& terrain,Player & p2, SAMPLE* son[SON_NB]);
void bitmap_destroyer(BITMAP* img[BMP_NB]);

void init_sound(SAMPLE* son[SON_NB]);
void sound_destroyer(SAMPLE* son[SON_NB]);
void sound(SAMPLE* son[SON_NB], int i);
