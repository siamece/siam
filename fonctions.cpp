#include "Animal.h"
#include "console.h"
#include "Entite.h"
#include "fonctions.h"
#include "main.h"
#include "Montagne.h"
#include "player.h"
#include "Terrain.h"
#include "jeu.h"
#include "graph.h"

using namespace std;

void creationPerso(Player& p1, Player& p2)
{
    string name;
    string name2;
    cout << "Joueur 1 : Veuillez entrer votre nom : " << endl;
        cin >> name;
        p1.setName(name);

    cout << "Joueur 2 : Veuillez entrer votre nom : " << endl;
        cin >> name2;
        p2.setName(name2);
}
void gestion_goto(int* lig, int* col, Console* pConsole)
{
    if (key[KEY_RIGHT]) col++;
    else if(key[KEY_LEFT]) col--;
    else if(key[KEY_UP]) lig++;
    else if(key[KEY_DOWN]) lig--;

    pConsole->gotoLigCol(*lig,*col);
}

////////////////////////////////////////////////
//Nom:getconsole_size
//Entrées :*POS_ECRAN_X,*POS_ECRAN_Y
//Sorties : +
//Date de dernière modification : 06/12/2015
//Nature de la dernière modif :Commentaire et correction des warnings
//Version : 2.0
//
//Issu du projet d'ing1 de Gabriel Padis
////////////////////////////////////////////////
void getconsole_size(int* POS_ECRAN_X, int* POS_ECRAN_Y)
{
    CONSOLE_SCREEN_BUFFER_INFO csbi;
    int colonnes, lignes;
    GetConsoleScreenBufferInfo(GetStdHandle(STD_OUTPUT_HANDLE),&csbi);
    colonnes=csbi.srWindow.Right-csbi.srWindow.Left+1;
    lignes = csbi.srWindow.Bottom-csbi.srWindow.Top+1;
    if (colonnes>87) *POS_ECRAN_X=colonnes/4;
    else *POS_ECRAN_X=0;
    *POS_ECRAN_Y = lignes/4;
}

void afficher(Terrain& terrain, Console* pConsole, BITMAP* img[BMP_NB])
{
    if(terrain.getGraphMode())
    {
        affichageGraph(img, terrain);
    }
    else terrain.afficher(0,0,pConsole);
}

void menuMode(Player& p, Console* pConsole, Terrain& terrain,Player& p2, SAMPLE* son[SON_NB])
{
  /*  if(terrain.getGraphMode())
    {
        menuGraph(p, img, terrain, p2, son);
    }
    else menu(p, pConsole, terrain, p2);
    */
    menu(p, pConsole, terrain,p2,son);
}

void menu(Player& p, Console* pConsole, Terrain& terrain,Player& p2, SAMPLE* son[SON_NB])
{
    int x = 21; //début du texte
    char choix = '0';
    bool valid = false;

    pConsole->gotoLigCol(x,0);
    cout << endl;
    cout << "C'est au tour de " << p.getName() << " : (" << p.getPiece() << ")" << endl;
    cout << "Quel est votre choix a ce tour ? " << endl;
    cout << "1) Rentrer un pion" << endl;
    cout << "2) Deplacer un pion" << endl;
    cout << "3) Sortir un pion" << endl;
    cout << "4) Changer l'orientation d'un pion" << endl;
    cout << "5) Abandonner" << endl;

    while(!valid)
    {
        cin >> choix;
        switch(choix)
        {
        case '1' : ajoutPion(p, terrain, pConsole,p2, son);
                 valid = true;
            break;
        case '2': deplacerPion(p, terrain, pConsole, p2, son);
                valid = true;
            break;
        case '3': sortiePion(p, terrain, pConsole);
                valid = true;
            break;
        case '4' : orientationPiece(p, terrain, pConsole);
                 valid = true;
            break;
        case '5' : terrain.won(p2);
                 valid = true;
            break;
        case '6' :lancementGraphMode(terrain);
            break;
        case '7' :
            for(int j = 0; j < SON_NB; j++)
            {
                stop_sample(son[j]);
            }
            break;
        default :
            break;
        }
    }
}

void deallocalisation(Terrain& terrain)
{
    for (const auto& elem : terrain.getDehorsElephant())
    {
        delete elem;
    }

    for (const auto& elem : terrain.getDehorsRhino())
    {
        delete elem;
    }

    for (const auto& col : terrain.getPlateau())
    {
        for(const auto& row : col)
        {
            delete row;
        }
    }
}


/*
Nom : bitmap_destroyer
Entrées : tableau d'iamges et buffer
Sorties :
Date de dernière modification : 14/03
Nature de la dernière modification : création par Gabriel Padis en ING1
Version : 1.0
*/
void bitmap_destroyer(BITMAP* img[BMP_NB])
{
    int i;
    for(i = 0; i < BMP_NB; i++)
    {
        destroy_bitmap(img[i]);
    }
}

/*
Nom : init_sound
Entrées :
Sorties :
Date de dernière modification : 01/05
Nature de la dernière modification : création par Gabriel Padis en ING1
Version : 1.0
*/
void init_sound(SAMPLE* son[SON_NB])
{
    int i;
    char son_name[SON_NB][10];


    for(i=0; i<SON_NB; i++)
    {
        son_name[i][0] = '\0';
        sprintf(son_name[i], "%d.wav", i);
        //printf("%s\n",son_name[i]);
        son[i] = load_sample(son_name[i]); //les sons sont appelés 1,2,3,... pour être facilement initialisée
        //if(!son[i]) printf("RIP sound\n");
    }
}

/*
Nom : sound_destroyer
Entrées :
Sorties :
Date de dernière modification : 01/05
Nature de la dernière modification : création par Gabriel Padis en ING1
Version : 1.0
*/
void sound_destroyer(SAMPLE* son[SON_NB])
{
    int i;
    for(i = 0; i < SON_NB; i++)
    {
        destroy_sample(son[i]);
    }
}

/*
Nom : sound
Entrées :
Sorties :
Date de dernière modification : 01/05
Nature de la dernière modification : création par Gabriel Padis en ING1
Version : 1.0
*/
void sound(SAMPLE* son[SON_NB], int i)
{
    int j;
    int non_son_piece = 1;//1 seul fichier son en musique de fond
    int sound_level = 120;
    if (i < non_son_piece)
    {
        for(j = 0; j < SON_NB; j++)
        {
            stop_sample(son[j]);
        }
        play_sample(son[i],sound_level,128,1000,1);
    }
    else //événementiel Elephant ou Rhinoceros
    {
        play_sample(son[i],sound_level+30,128,1000,0);
    }
}
