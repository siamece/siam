#include "main.h"

void lancementGraphMode(Terrain& terrain);
void affichageGraph(BITMAP* img[BMP_NB], Terrain& terrain);
void bitmap_loader(BITMAP* img[BMP_NB]);
bool actionCase(Terrain& terrain, Player& p, Player& p2, SAMPLE* son[BMP_NB]);
void orientation(BITMAP* buffer, BITMAP* image, int x, int y, std::vector<std::vector<Entite*>>& plateau);
void menuGraph(Player& p, Terrain& terrain, Player& p2, SAMPLE* son[BMP_NB]);
void graphPlacement(BITMAP* buffer, BITMAP* img[BMP_NB], Terrain& terrain);

