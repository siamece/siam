#ifndef LECTURE_H_INCLUDED
#define LECTURE_H_INCLUDED
#include <iostream>
#include <fstream>
// Lire un fichier ligne par ligne
// Entr�e : le chemin d'acc�s au fichier
void readFile(const std::string& path);

void writeFile(const std::string& path);

#endif // LECTURE_H_INCLUDED
