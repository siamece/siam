//#include "fonctions.h"
#include "Terrain.h"
#include "graph.h"
#include "jeu.h"


/*
Nom : bitmap_loader
Entr�es : tableau d'images
Sorties :
Date de derni�re modification : 14/03
Origine : Projet du second semstre d'ING1 de Gabriel Padis
Version : 1.0
*/
void bitmap_loader(BITMAP* img[BMP_NB])
{
    int i=0;
    char bmp_name[BMP_NB][10];

    for( i = 0;  i < BMP_NB; i++)
    {
        bmp_name[i][0] = '\0';
        sprintf(bmp_name[i], "%d.bmp", i);
        //printf("%s\n",bmp_name[i]);
        img[i] = load_bitmap(bmp_name[i],NULL); //les images sont appel�s 1,2,3,... pour �tre facilement initialis�e
        //if(!img[i]) printf("RIP img\n");
    }
}

void lancementGraphMode(Terrain& terrain)
{
    if (set_gfx_mode(GFX_AUTODETECT_WINDOWED,WIDTH,HEIGHT,0,0)!=0)
    {
        allegro_message("problem graphic mode");
        allegro_exit();
        exit(EXIT_FAILURE);
    }
    show_mouse(screen);
    terrain.setGraphMode(true);
}

void affichageGraph(BITMAP* img[BMP_NB], Terrain& terrain)
{
    BITMAP* buffer =  create_bitmap(WIDTH,HEIGHT);
    blit(img[0],buffer,0,0,0,0,WIDTH,HEIGHT);
    graphPlacement(buffer, img, terrain);
    blit(buffer,screen,0,0,0,0,WIDTH,HEIGHT);
}

bool caseVide(int& x, int& y, Terrain& terrain)
{
//    int xtest = x;
//    int ytest = y;

    auto plateau = terrain.getPlateau();
    if (! (plateau[x][y] == nullptr))
    {
        //if ( (plateau[x][y]->getSymbole() == 'E') || (plateau[x][y]->getSymbole() == 'R') ) return false;
        return false;
    }
    else return true;

}


bool actionCase(Terrain& terrain, Player& p, Player& p2, SAMPLE* son[BMP_NB])
{

    int x, xo, y, yo;
    xo=0;
    yo=0;
    char orient=' ';
    bool first = false;
    bool second = false;
    bool third = false;
    bool rentrerE = false;
    bool rentrerR = false;

    auto plateau = terrain.getPlateau();

    if( p.getPiece() == "Elephant") std::cout << "Elephant" <<std::endl;
    if( p.getPiece() == "Rhinoceros") std::cout << "Rhinoceros" <<std::endl;
    /* if( (mouse_x >= 200) && (mouse_x <= 790) && (mouse_y >= 30) && (mouse_y <= 625) )
     {
         x = (mouse_x - 200)/T;
         y = (mouse_y - 30)/T;
     }
    */



    // CLIQUE GAUCHE SUR LE TERRAIN
    //CHANGE L ORIENTATION OU FAIS UN DEPLACEMENT

    if((mouse_b & 1) && (mouse_x >= 200) && (mouse_x <= 790) && (mouse_y >= 30) && (mouse_y <= 625)) //clique gauche sur le terrain
    {
        //si case non vide s�lection de la piece
        if(!caseVide(x,y,terrain))
        {
            x = (mouse_x - 200)/T;
            y = (mouse_y - 30)/T;
            std::cout << "selection" << std::endl;

            //ACTION

            while(!third)
            {
                //ORIENTATION
                std::cout << "orient" << std::endl;
                if( (mouse_b & 1) && (terrain.getPlateau()[y][x]->getSymbole() == p.getPiece().at(0)) )//r�cup�re ensuite l'orientation
                {
                    std::cout << "clique gauche orient" << std::endl;
                    /*if( (mouse_x >= 200) && (mouse_x <= 790) && (mouse_y >= 30) && (mouse_y <= 625) )
                    {
                        xo = (mouse_x - 200)/T;
                        yo = (mouse_y - 30)/T;
                        std::cout << xo << std::endl;
                        std::cout << yo << std::endl;
                    }*/

                    if(key[KEY_UP])

                    {
                        orient = 'h';
                        third = true;
                        if(plateau[x][y]->getSymbole() == p.getPiece().at(0)) plateau[y][x]->setOrientation(orient);
                    }
                    if(key[KEY_DOWN])
                    {
                        orient = 'b';
                        third = true;
                        if(plateau[x][y]->getSymbole() == p.getPiece().at(0)) plateau[y][x]->setOrientation(orient);
                    }
                    if(key[KEY_RIGHT])
                    {
                        orient = 'd';
                        third = true;
                        if(plateau[x][y]->getSymbole() == p.getPiece().at(0)) plateau[y][x]->setOrientation(orient);
                    }
                    if(key[KEY_LEFT])
                    {
                        //std::cout << "yo" << std::endl;
                        orient = 'g';
                        third = true;
                        if(plateau[x][y]->getSymbole() == p.getPiece().at(0)) plateau[y][x]->setOrientation(orient);
                    }

                    /*if( (xo > x) && (abs(xo-x) > abs(yo-y)) ) orient = 'd';
                    else if( (xo < x) && (abs(xo-x) > abs(yo-y)) ) orient = 'g';
                    else if( (yo > y) && (abs(xo-x) < abs(yo-y)) ) orient = 'b';
                    else if( (yo < y) && (abs(xo-x) < abs(yo-y)) ) orient = 'h';*/
                    std::cout << orient << std::endl;
                }

                //DEPLACEMENT
                else if (mouse_b & 2)
                {
                    ///initialisation de variables
                    int x0=x;
                    int y0=y;
                    int x1 = 0;
                    int y1 = 0;
                    char orient = ' ';
                    std::string data;
                    std::string data2;



                    bool caseAdja=false;

                    ///Si le pion que le joueur veut jouer est jouable par lui
                    //if(terrain.getPlateau()[x0][y0]->getSymbole() == p.getPiece().at(0))
                    if(terrain.getPlateau()[y0][x0]->getSymbole() == p.getPiece().at(0))
                    {
                        ///saisi de la case destination
                        //+blindage de si la case est adjacente
                        do
                        {
                            if( (mouse_x >= 200) && (mouse_x <= 790) && (mouse_y >= 30) && (mouse_y <= 625) )
                            {
                                x1 = (mouse_x - 200)/T;
                                y1 = (mouse_y - 30)/T;
                                std::cout << x1 << std::endl;
                                std::cout << y1 << std::endl;
                            }
                            //caseAdja=caseEstAdjacente(x0,y0,x1,y1);
                            caseAdja=caseEstAdjacente(y0,x0,y1,x1);
                            if(!caseAdja ) std::cout<<"Erreur ! Vous ne pouvez que vous deplacer d'une case ! "<<std::endl;
                        }
                        while(!caseAdja );

                        ///si la case destination est vide
                        //if(terrain.getPlateau()[x1][y1]==nullptr)
                        if(terrain.getPlateau()[y1][x1]==nullptr)
                        {
                            //alors on utilise deplacer sur case vide
                            //saisie de l'orientation
                            if( (x1 > x0) && (abs(x1-x0) > abs(y1-y0)) ) orient = 'd';
                            else if( (x1 < x0) && (abs(x1-x0) > abs(y1-y0)) ) orient = 'g';
                            else if( (y1 > y0) && (abs(x1-x0) < abs(y1-y0)) ) orient = 'b';
                            else if( (y1 < y0) && (abs(x1-x0) < abs(y1-y0)) ) orient = 'h';
                            //terrain.deplacer_sur_case_vide(x0,y0,x1,y1,orient);
                            terrain.deplacer_sur_case_vide(y0,x0,y1,x1,orient);
                        }
                        ///Sinon on utilise la fonction pousser
                        else
                        {
                            std::cout<<"pousse pousse"<<std::endl;
                            system("pause");

                            //switch(terrain.getPlateau()[x0][y0]->getOrientation())
                            switch(terrain.getPlateau()[y0][x0]->getOrientation())
                            {
                            case 'h':
                                std::cout<<"x0="<<x0<<std::endl;
                                std::cout<<"y0="<<y0<<std::endl;
                                //if (x1==x0-1) terrain.deplacer_en_poussant(y0,x0,'h',p,p2);
                                if (x1==x0-1) terrain.deplacer_en_poussant(x0,y0,'h',p,p2,son);
                                else std::cout<<"Vous ne pouvez pousser qu'en avant"<<std::endl;
                                break;
                            case 'b':
                                //if (x1==x0+1) terrain.deplacer_en_poussant(y0,x0,'b',p,p2);
                                if (x1==x0+1) terrain.deplacer_en_poussant(x0,y0,'b',p,p2,son);
                                else std::cout<<"Vous ne pouvez pousser qu'en avant"<<std::endl;
                                break;
                            case 'g':
                                //if (y1==y0-1) terrain.deplacer_en_poussant(y0,x0,'g',p,p2);
                                if (y1==y0-1) terrain.deplacer_en_poussant(x0,y0,'g',p,p2,son);
                                else std::cout<<"Vous ne pouvez pousser qu'en avant"<<std::endl;
                                break;
                            case 'd':
                                std::cout<<"x0="<<x0<<std::endl;
                                std::cout<<"y0="<<y0<<std::endl;
                                //if (y1==y0+1) terrain.deplacer_en_poussant(y0,x0,'d',p,p2);
                                if (y1==y0+1) terrain.deplacer_en_poussant(x0,y0,'d',p,p2,son);
                                else std::cout<<"Vous ne pouvez pousser qu'en avant"<<std::endl;
                                break;
                            }
                            system("pause");

                        }
                    }
                    else
                    {
                        std::cout<<"Vous ne pouvez pas deplacer un";
                        //if(terrain.getPlateau()[x0][y0]->getSymbole()=='E') std::cout<<" Elephant car vous controlez les Rhinoceros"<<std::endl;
                        if(terrain.getPlateau()[y0][x0]->getSymbole()=='E') std::cout<<" Elephant car vous controlez les Rhinoceros"<<std::endl;
                        else std::cout<<" Rhinoceros car vous controlez les Elephants"<<std::endl;
                        system("pause");
                    }
                third = true;
                }
            }

            return false;//le joueur a jou�
        }
    }


    //ENTREE ET SORTIE

    //si on s�lectionne les Elephants et qu'on est le joueur Elephant
    if((mouse_b & 1) && (mouse_x <= 190) && ( p.getPiece() == "Elephant"))
    {
        rentrerE = true;
        std::cout << "Elephant selectionne" << std::endl;
    }
    //si on s�lectionne les Rhinoc�ros et qu'on est le joueur Rhionoc�ros
    if((mouse_b & 1) && (mouse_x >= 800) && ( p.getPiece() == "Rhinoceros"))
    {
        rentrerR = true;
        std::cout << "Rhinoceros selectionne" << std::endl;
    }
    //std::cout << "E : " << rentrerE << std::endl;
    //std::cout << "R : " << rentrerR << std::endl;
    if(rentrerE || rentrerR)
    {
        while(!first)
        {
            if( (mouse_b & 2) && (mouse_x >= 200) && (mouse_x <= 790) && (mouse_y >= 30) && (mouse_y <= 625)) //clique droit sur le terrain pour ajouter ou sortir une pi�ce
            {
                std::cout << "clique droit 1" << std::endl;

                if( (mouse_x >= 200) && (mouse_x <= 790) && (mouse_y >= 30) && (mouse_y <= 625) )//r�cup�re la case o� le joueur � cliquer
                {
                    x = (mouse_x - 200)/T;
                    y = (mouse_y - 30)/T;
                    std::cout << x << std::endl;
                    std::cout << y << std::endl;
                }
                while(!second )
                {
                    //ENTRATION
                    if((mouse_b & 1) && (mouse_x >= 200) && (mouse_x <= 790) && (mouse_y >= 30) && (mouse_y <= 625))//r�cup�re ensuite l'orientation
                    {
                        std::cout << "clique gauche 2" << std::endl;
                        if( (mouse_x >= 200) && (mouse_x <= 790) && (mouse_y >= 30) && (mouse_y <= 625) )
                        {
                            xo = (mouse_x - 200)/T;
                            yo = (mouse_y - 30)/T;
                            std::cout << xo << std::endl;
                            std::cout << yo << std::endl;
                        }
                        if( (xo > x) && (abs(xo-x) > abs(yo-y)) ) orient = 'd';
                        else if( (xo < x) && (abs(xo-x) > abs(yo-y)) ) orient = 'g';
                        else if( (yo > y) && (abs(xo-x) < abs(yo-y)) ) orient = 'b';
                        else if( (yo < y) && (abs(xo-x) < abs(yo-y)) ) orient = 'h';
                        std::cout << orient << std::endl;
                        std::cout << "E : " << rentrerE << std::endl;
                        std::cout << "R : " << rentrerR << std::endl;
                        if(rentrerE)
                        {
                            terrain.faire_entrer_animal('E',orient,y,x,p,p2,son);
                            std::cout << "Succes Elephant" << std::endl;
                        }
                        else if(rentrerR)
                        {
                            terrain.faire_entrer_animal('R',orient,y,x,p,p2,son);
                            std::cout << "Succes Rhinoceros" << std::endl;
                        }
                        system("pause");

                    }


                    //SORTATION
                    else if ( (mouse_b & 1) && ( (mouse_x <= 190) || (mouse_x >= 800)))//en dehors du terrain
                    {
                        if( p.getPiece() == "Elephant") terrain.faire_sortir_animal(p,x,y);
                        if( p.getPiece() == "Rhinoceros") terrain.faire_sortir_animal(p,x,y);
                    }
                    second = true;
                }
            }
            return false;
        }
    }
    return true;
}


void menuGraph(Player& p, Terrain& terrain, Player& p2, SAMPLE* son[BMP_NB])
{
    bool turn = true;

    while(!key[KEY_ESC])// && turn)
    {
        turn = actionCase(terrain, p, p2,son);
        std::cout<<turn<<std::endl;
        rest(500);
    }
    if(key[KEY_ESC]) terrain.setGraphMode(false);

}


void orientation(BITMAP* buffer, BITMAP* image, int x, int y, std::vector<std::vector<Entite*>>& plateau, int i, int j)
{
    if(plateau[i][j]->getOrientation() == 'd') draw_sprite(buffer, image, x, y);
    if(plateau[i][j]->getOrientation() == 'g') draw_sprite_h_flip(buffer, image, x, y);
    if(plateau[i][j]->getOrientation() == 'b') rotate_sprite(buffer, image, x, y, ftofix(64)); //va vers le bas
    if(plateau[i][j]->getOrientation() == 'h') rotate_sprite(buffer, image, x, y, ftofix(-64)); // va vers le haut
}


void graphPlacement(BITMAP* buffer, BITMAP* img[BMP_NB], Terrain& terrain) //200 et 30
{
    int X = 200;
    int Y = 30;

    // int T = 115; //tailles des carr�s

    auto plateau = terrain.getPlateau();

    for (int i = 0; i < 5; i ++)
    {
        for (int j = 0; j < 5; j ++)
        {
            int posy = i*T + Y + i*7;
            int posx = j*T + X + j*7;
            if(! (plateau[i][j] == nullptr))
            {
                if(plateau[i][j]->getSymbole() == 'R') orientation(buffer, img[1], posx, posy, plateau,i,j);
                if(plateau[i][j]->getSymbole() == 'E') orientation(buffer, img[2], posx, posy, plateau,i,j);
                if(plateau[i][j]->getSymbole() == 'M') draw_sprite(buffer, img[3], posx, posy);
            }
        }
    }

    //affichage de ceux en dehors
    //Elephant
    int xE = 50;
    for(unsigned int i=0;i<terrain.getDehorsElephant().size();i++)
    {
        draw_sprite(buffer, img[2], xE, Y);
        Y += T;
    }
//    for (const auto& elem : terrain.getDehorsElephant())
//    {
//        draw_sprite(buffer, img[2], xE, Y);
//        Y += T;
//    }

    //Rhino
    int xR = 950-T;
    Y = 30;
    for(unsigned int i=0;i<terrain.getDehorsRhino().size();i++)
    {
        draw_sprite(buffer, img[1], xR, Y);
        Y += T;
    }
//    for(const auto& elem2 : terrain.getDehorsRhino())
//    {
//        draw_sprite(buffer, img[1], xR, Y);
//        Y += T;
//    }

  //  if(p.getPiece() == "Elephant") circle(buffer,100, 600, 15, makecol(255,0,0));
  //  if(p.getPiece() == "Rhinoceros") circle(buffer,900, 600, 15, makecol(255,0,0));
}
