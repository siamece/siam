#include "Animal.h"
#include "console.h"
#include "Entite.h"
#include "fonctions.h"
#include "main.h"
#include "Montagne.h"
#include "player.h"
#include "Terrain.h"
#include "graph.h"

using namespace std;

int main()
{
    Console* pConsole = NULL;
    pConsole = Console::getInstance();

    int POS_ECRAN_Y=0;
    int POS_ECRAN_X=0;

    getconsole_size(&POS_ECRAN_X, &POS_ECRAN_Y);

    //code gotoligcol donn�

    // Affichage avec gotoligcol et couleur
    pConsole->gotoLigCol(POS_ECRAN_X/4-3, POS_ECRAN_Y);
    pConsole->setColor(COLOR_GREEN);
    std::cout << "Bienvenue dans SIAM !" << std::endl;
    pConsole->setColor(COLOR_DEFAULT);

    //ALLEGRO
    srand(time(NULL));
    allegro_init();
    install_keyboard();
    install_mouse();
    install_sound(DIGI_AUTODETECT , MIDI_AUTODETECT ,0);
    set_uformat(U_ASCII);
    set_color_depth(desktop_color_depth());


    //jeu

    BITMAP* img[BMP_NB];
    bitmap_loader(img); // charge toutes les images

    SAMPLE* son[SON_NB]; //Issu du projet de Gabriel Padis
    init_sound(son);

    Terrain terrain;

    int tour = 0;
    bool esc = false;



    Player p1("Elephant");
    Player p2("Rhinoceros");

    creationPerso(p1, p2);
    terrain.initialisationPlateau();

    system("cls");

    terrain.afficher(0,0,pConsole);

    sound(son,0);

    while((!terrain.getIsOver()) && (!esc))
    {
        if (tour == 0)
        {
            menuMode(p1, pConsole, terrain, p2, son);//tour de joueur 1
            sound(son,2);
        }
        else
        {
            menuMode(p2, pConsole, terrain, p1, son);// tour de joueur 2
            sound(son,3);
        }
        if (pConsole->isKeyboardPressed())
        {
            int key = pConsole->getInputKey();
            if (key == 27) esc = true;
            else esc = false;
        }
        system("cls");
        afficher(terrain, pConsole, img);
        tour++;
        rest(150);
        tour = tour%2;
    }
    if(p1.getHasWon()==true)
    {
        pConsole->gotoLigCol(18,0);
        pConsole->setColor(COLOR_GREEN);
        std::cout<<"Le joueur "<<p1.getName()<<" a gagne !"<<std::endl;
        pConsole->setColor(COLOR_DEFAULT);
        system("pause");
    }
    if(p2.getHasWon()==true)
    {
        pConsole->gotoLigCol(18,0);
        pConsole->setColor(COLOR_GREEN);
        std::cout<<"Le joueur "<<p2.getName()<<" a gagne !"<<std::endl;
        pConsole->setColor(COLOR_DEFAULT);
        system("pause");
    }


    system("cls");
    cout << "Liberation de la memoire ... " << endl;



    // Lib�re la m�moire du pointeur !
    Console::deleteInstance();

    //Liberation des pieces
    deallocalisation(terrain);

    //liberation des images
    bitmap_destroyer(img);

    //liberation du son
    sound_destroyer(son);

    return 0;
}END_OF_MAIN();
