#ifndef PLAYER_H
#define PLAYER_H
#include "main.h"

class Player
{
private :
    std::string m_name;
    int m_score = 0;
    std::string m_piece;
    bool m_hasPlayed = false;
    bool m_hasWon= false;


public :
    Player();
    Player(std::string _piece);
    ~Player();

    //getters and setters
    std::string getName() const;
    std::string getPiece() const;
    int getScore() const;
    bool getHasPlayed() const;
    bool getHasWon() const;

    void setName(std::string name);
    void setScore(int score);
    void setHasPlayed(bool play);
    void setHasWon(bool haswon);

    //methods
};

#endif
