#ifndef ANIMAL_H
#define ANIMAL_H
#include "main.h"
#include "Entite.h"

class Animal : public Entite
{
    public:
        Animal();
        Animal(std::string _nom);
        Animal(char symbole,std::string nom);
        virtual ~Animal();

        std::string getNom() const;
        void setNom(std::string _nom);



    protected:

    private:
//        std::string m_nom;
};

#endif // ANIMAL_H
