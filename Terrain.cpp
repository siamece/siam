#include "Terrain.h"
#include "Animal.h"
#include "console.h"
#include "Entite.h"
#include "fonctions.h"
#include "main.h"
#include "Montagne.h"
#include "player.h"
#include "lecture.h"
#define LIGNE 5
#define COLONNE 5

using namespace std;

Terrain::Terrain()
{

}

Terrain::~Terrain()
{
    //dtor
}

vector<vector<Entite*>> Terrain::getPlateau() const
{
    return m_plateau;
}

void Terrain::setPlateau(vector<vector<Entite*>> plateau)
{
        m_plateau=plateau;

}

bool Terrain::getIsOver() const
{
    return m_isOver;
}

bool Terrain::getGraphMode() const
{
    return m_graph_mode;
}

void Terrain::setIsOVer(bool b)
{
    m_isOver = b;
}

void Terrain::setGraphMode(bool b)
{
    m_graph_mode = b;
}


void Terrain::won(Player & p)
{
    setIsOVer(true);
    p.setHasWon(true);
}

void Terrain::initialisationPlateau()
{
    m_plateau.resize(6, vector<Entite*>(1 << 5,0));

    for (int i = 0; i < 5; i ++)
    {
        for (int j = 0; j < 5; j ++)
        {
            m_plateau[i][j] = nullptr;
        }
    }
    //test
    //mise en place des montagnes
    m_plateau[1][2] = new Montagne();
    m_plateau[2][2] = new Montagne();
    m_plateau[3][2] = new Montagne();
//    ///tests pour la pouss�e
//    m_plateau[1][0] = new Montagne(3,2);

    //remplissage du tableau m_dehors_elephant car aucun pion n'a encore �t� mis
    m_dehors_elephant.push_back( new Animal('E',"Ele1") );
    m_dehors_elephant.push_back( new Animal('E',"Ele2") );
    m_dehors_elephant.push_back( new Animal('E',"Ele3") );
    m_dehors_elephant.push_back( new Animal('E',"Ele4") );
    m_dehors_elephant.push_back( new Animal('E',"Ele5") );

    m_dehors_rhino.push_back( new Animal('R',"Rhi1") );
    m_dehors_rhino.push_back( new Animal('R',"Rhi2") );
    m_dehors_rhino.push_back( new Animal('R',"Rhi3") );
    m_dehors_rhino.push_back( new Animal('R',"Rhi4") );
    m_dehors_rhino.push_back( new Animal('R',"Rhi5") );

    //a enlever apres car quand on commence le terrain est vide
    /*m_plateau[0][0] = new Animal("Elephant",0,0);
    m_plateau[1][0] = new Animal("Elephant",1,0);
    m_plateau[2][0] = new Animal("Elephant",2,0);
    m_plateau[3][0] = new Animal("Elephant",3,0);
    m_plateau[4][0] = new Animal("Elephant",4,0);

    m_plateau[0][4] = new Animal("Rhinoceros",0,4);
    m_plateau[1][4] = new Animal("Rhinoceros",1,4);
    m_plateau[2][4] = new Animal("Rhinoceros",2,4);
    m_plateau[3][4] = new Animal("Rhinoceros",3,4);
    m_plateau[4][4] = new Animal("Rhinoceros",4,4);
//    m_plateau[0][0] = new Animal("Elephant",0,0);
//    m_plateau[1][0] = new Animal("Elephant",1,0);
//    m_plateau[2][0] = new Animal("Elephant",2,0);
//    m_plateau[3][0] = new Animal("Elephant",3,0);
//    m_plateau[4][0] = new Animal("Elephant",4,0);
//
//    m_plateau[0][4] = new Animal("Rhinoceros",0,4);
//    m_plateau[1][4] = new Animal("Rhinoceros",1,4);
//    m_plateau[2][4] = new Animal("Rhinoceros",2,4);
//    m_plateau[3][4] = new Animal("Rhinoceros",3,4);
//    m_plateau[4][4] = new Animal("Rhinoceros",4,4);



    //code obselete
    for (int i = 0; i < 5; i ++)
    {
        for (int j = 0; j < 5; j ++)
        {
            m_plateau_2[i][j] = new Entite() ;
        }
    }

    m_plateau_2[0][0] = new Animal("Elephant",0,1);
    m_plateau_2[1][0] = new Animal("Elephant",0,1);
    m_plateau_2[2][0] = new Animal("Elephant",0,2);
    m_plateau_2[3][0] = new Animal("Elephant",0,3);
    m_plateau_2[4][0] = new Animal("Elephant",0,4);

    m_plateau_2[1][2] = new Montagne(2,1);
    m_plateau_2[2][2] = new Montagne(2,2);
    m_plateau_2[3][2] = new Montagne(2,3);

    m_plateau_2[0][4] = new Animal("Rhinoceros",4,0);
    m_plateau_2[1][4] = new Animal("Rhinoceros",4,1);
    m_plateau_2[2][4] = new Animal("Rhinoceros",4,2);
    m_plateau_2[3][4] = new Animal("Rhinoceros",4,3);
    m_plateau_2[4][4] = new Animal("Rhinoceros",4,4);
    */
}

void Terrain::afficher(int pos_x, int pos_y, Console* pConsole)
{
    // posx posy sont les coords o� commencer a afficher le plateau
    // x et y sont coords relatives au plateau pour placer les pieces
    int x,y;
    ///affichage des bords du plateau
    pConsole->gotoLigCol(pos_x, pos_y);
    readFile("Terrain.txt");
    ///affichage du plateau de jeu
    //POSITION du curseur sur le plateau du milieu

    y=pos_y+3; // coords de la premiere case
    x=pos_x+18; //
    pConsole->gotoLigCol(y, x);
    //parcours du vecteur de pieces
    for (int i = 0; i < 5; i ++)
    {
        for (int j = 0; j < 5; j ++)
        {
            pConsole->gotoLigCol(y,x);
            pConsole->setColor(COLOR_WHITE);
            if(m_plateau[i][j]!=nullptr)
            {
                std::cout << m_plateau[i][j]->getSymbole();
                pConsole->setColor(COLOR_GREEN);
                std::cout<<m_plateau[i][j]->getOrientation();
            }

            x+=5;

        }
        //remet le x au debut
        x=pos_x+18;
        y+=3;
        pConsole->gotoLigCol(y, x);
    }
    //Set color pour les pieces en dehors
    pConsole->setColor(COLOR_YELLOW);
    ///afficher les pi�ces en dehors pour elephant

    x=pos_x+2;
    y=pos_y+3;
    pConsole->gotoLigCol(y,x);
    for(auto elem : m_dehors_elephant)
    {
        std::cout<<elem->getSymbole();
        y+=3;
        pConsole->gotoLigCol(y,x);
    }
    ///afficher les pi�ces en dehors pour le rhinos
    x=pos_x+49;
    y=pos_y+3;
    pConsole->gotoLigCol(y, x);
    for(const auto& elem : m_dehors_rhino)
    {
        std::cout<<elem->getSymbole();
        y+=3;
        pConsole->gotoLigCol(y,x);
    }
    pConsole->setColor(COLOR_DEFAULT);
}

void Terrain::faire_entrer_animal(char symbole,char orientation,unsigned int x ,unsigned int y,Player& p1, Player& p2, SAMPLE* son[BMP_NB])
{
    bool GoodToPush=false;
    //Verification de x et y pour qu'ils soient bien conforme aux bords du plateau
    if((y==0 && x<=4) || (y==4 && x<=4) || (y<=4 && x==0) ||(y<=4 && x==4))
    {
        if(symbole=='E')
        {
        //je regarde si y'en a dans m_dehors
        //si ya je le deplace , de m_dehors � m_plateau
            if(!m_dehors_elephant.empty())
            {
                //si la case o� on veut aller est vide
                if(m_plateau[x][y]== nullptr)
                {
                    //je prend le dernier animal de m'dehors
                    m_plateau[x][y]=m_dehors_elephant.back();
                    //je met l'orientation
                    m_plateau[x][y]->setOrientation(orientation);
                    //
                    m_dehors_elephant.pop_back();
                }
                else{
                    //test si il peut entrer en poussant avec son orientation
                    if(y==0 && orientation=='d') GoodToPush=true;
                    if(y==4 && orientation=='g') GoodToPush=true;
                    if(x==0 && orientation=='b') GoodToPush=true;
                    if(x==4 && orientation=='h') GoodToPush=true;

                    if(GoodToPush)
                    {
                        deplacer_en_poussant(y,x,orientation,p1,p2,son, true);
                        //je prend le dernier animal de m'dehors
                        m_plateau[x][y]=m_dehors_elephant.back();
                        //je met l'orientation
                        m_plateau[x][y]->setOrientation(orientation);
                        //
                        m_dehors_elephant.pop_back();
                    }
                    else
                    {
                        std::cout<<"Vous ne pouvez pas entrer en ayant cette orientation voyons... vous n'avez pas assez de force orientee comme ceci! "<<std::endl;
                        std::cout<<"Et hop vous venez de gacher un tour :) !"<<std::endl;
                        system("pause");
                    }

                }
            }
        }
        if(symbole=='R')
        {
        //je regarde si y'en a dans m_dehors
        //si ya je le deplace , de m_dehors � m_plateau
            if(!m_dehors_rhino.empty())
            {
                if(m_plateau[x][y]==nullptr)
                {
                    m_plateau[x][y]=m_dehors_rhino.back();
                    m_plateau[x][y]->setOrientation(orientation);
                    m_dehors_rhino.pop_back();
                }
                else{
                    //test si il peut entrer en poussant avec son orientation
                    if(y==0 && orientation=='d') GoodToPush=true;
                    if(y==4 && orientation=='g') GoodToPush=true;
                    if(x==0 && orientation=='b') GoodToPush=true;
                    if(x==4 && orientation=='h') GoodToPush=true;

                    if(GoodToPush)
                    {
                        deplacer_en_poussant(y,x,orientation,p1,p2,son, true);
                        //je prend le dernier animal de m'dehors
                        m_plateau[x][y]=m_dehors_rhino.back();
                        //je met l'orientation
                        m_plateau[x][y]->setOrientation(orientation);
                        //
                        m_dehors_rhino.pop_back();
                    }
                    else
                    {
                        std::cout<<"Vous ne pouvez pas entrer en ayant cette orientation voyons... vous n'avez pas assez de force orientee comme ceci ! "<<std::endl;
                        std::cout<<"Et hop vous venez de gacher un tour :) !"<<std::endl;
                        system("pause");
                    }
                }
            }
        }
    }
    else{
        std::cerr<<"Erreur : Le pion ne peut entrer que par les bords du plateau"<<std::endl;
        std::cerr<<"Vous venez de gacher un tour :/"<<std::endl;
        system("pause");
    }
}
void Terrain::faire_sortir_animal(const Player& j,unsigned int x,unsigned int y)
{
    bool isBordure=false;
    bool isBonAnimal=false;
    isBordure=((y==0 && x<=4) || (y==4 && x<=4) || (y<=4 && x==0) ||(y<=4 && x==4));
    cout << isBordure << endl;

    if(! (m_plateau[x][y] == nullptr))
    {
    isBonAnimal= (m_plateau[x][y]->getSymbole()==j.getPiece().at(0));
    }
    cout << isBonAnimal << endl;
    //while(!isBordure)
    //Verification de x et y pour qu'ils soient bien conforme aux bords du plateau
    if(isBordure)
    {
        //si le joeueur peux bouger cette piece
        if(isBonAnimal)
        {
            //on regarde de quelle piece il s'agit
            if(m_plateau[x][y]->getSymbole()=='E')
            {
                m_dehors_elephant.push_back(m_plateau[x][y]);
                m_plateau[x][y]=nullptr;
            }
            else
            {
                m_dehors_rhino.push_back(m_plateau[x][y]);
                m_plateau[x][y]=nullptr;
            }
        }
        else std::cout<<j.getName()<<" doit jouer "<<j.getPiece()<<std::endl;

    }
    else{
        std::cerr<<"Erreur : Le pion ne peut etre retire quand il n'est pas sur les bords du tableau"<<std::endl;
        std::cerr<<"Vous venez de gacher un tour :/"<<std::endl;
        system("pause");
    }
    //rest(1000);
}
void Terrain::deplacer_sur_case_vide(int x0,int y0,int x1,int y1,char orientation)
{
    //je v�rifie bien que la case o� on veut se deplacer est vide
    if((x1==x0+1 )^ (x1==x0-1) ^ (y1==y0+1) ^ (y1==y0-1))
    {
        if(m_plateau[x1][y1]==nullptr || m_plateau[x1][y1]->getSymbole()==' ')
        {
//            std::cout<<"OKAY1"<<std::endl;
            m_plateau[x1][y1]= m_plateau[x0][y0];
//            std::cout<<"OKAY2"<<std::endl;
            m_plateau[x0][y0]=nullptr;
//            std::cout<<"OKAY3"<<std::endl;
            m_plateau[x1][y1]->setOrientation(orientation);
//            std::cout<<"OKAY4"<<std::endl;
        }
        else std::cout<<"erreur 456"<<std::endl;

    }
    else{
        std::cerr<<"La case ou vous voulez vous deplacer n'est pas vide OU n'est pas orthogonal a la case d'origine"<<std::endl;
    }

}
void Terrain::calcLigColAvecOrientation(int & x,int & y, char orientation)
{
    if(orientation=='h') y-=1;
    if(orientation=='b') y+=1;
    if(orientation=='g') x-=1;
    if(orientation=='d') x+=1;
}

char Terrain::calcOrientationOpposee(char& o)
{
    char o_p;
    if(o=='h') o_p='b';
    if(o=='b') o_p='h';
    if(o=='g') o_p='d';
    if(o=='d') o_p='g';
    return o_p;
}
bool Terrain::calcisInTab(int x,int y)
{
    return (x>=0 && x<=4 && y>=0 && y<=4);
}

void Terrain::deplacer_en_poussant(int x0,int y0,char orientation,Player &p1,Player &p2,SAMPLE* son[BMP_NB], bool isEntrying)
{

    //C'est v�rifi� en amont que la case que l'on veut pousser n'est pas vide
    // et que il est dans la bonne direction
    float force=0;
    if(isEntrying) force =1;
    float resistance=0;
    int i=y0; //i et j sont les iactuel et jactuel
    int j=x0;
    int itemp;//
    int jtemp;
    bool isInTab=true;
    bool sameOrient=true;

    //orientation oppos�e
    char orientation_opp=calcOrientationOpposee(orientation);
    ///Re�criture du code
//    std::cout<<"Debut force = "<<force<<" resistance ="<<resistance<<" i ="<<i<<" j ="<<j<<std::endl;
//    std::cout<<std::endl;
//    while(m_plateau[i][j]!=nullptr || calcisInTab(j,i))
//    {
//        char symb=m_plateau[i][j]->getSymbole();
//        if(orientation==m_plateau[i][j]->getOrientation())
//        {
//            force+=1;
//        }
//        else
//        {
//
//            if(symb=='M') resistance+=0.9;
//            if(symb=='E' || symb=='R')
//            {
//                if(m_plateau[i][j]->getOrientation()==orientation_opp) resistance+=1;
//            }
//        }
//        calcLigColAvecOrientation(j,i,orientation);
//        std::cout<<"Fin 1ertour avec force = "<<force<<" resistance "<<resistance<<" i "<<i<<" j "<<j<<std::endl;
//    }
    if(m_plateau[i][j]!=nullptr)    sameOrient=(orientation==m_plateau[i][j]->getOrientation());
    else sameOrient=false;
//    if(isEntrying)
//    {
//        calcLigColAvecOrientation(j,i,orientation_opp);
//        std::cout<<"is entering i="<<i<<" j="<<j<<std::endl;
//    }
    ///Boucle de calcul des forces
    while((sameOrient && isInTab ) && m_plateau[i][j]!=nullptr)
    {
//        std::cout<<"debut tour du boucle des forces avec force = "<<force<<" resistance "<<resistance<<" i "<<i<<" j "<<j<<std::endl;

        force+=1;

        //incremente i ou j en fonction de l'orientation
        calcLigColAvecOrientation(j,i,orientation);

        isInTab=calcisInTab(j,i);
        if(isInTab)
        {
//            std::cout<<"if m_plateau!=nullptr"<<std::endl;
            if(m_plateau[i][j]!=nullptr)
                sameOrient=(orientation==m_plateau[i][j]->getOrientation());
        }
        else
        {
//            std::cout<<"else m_plateau==nullptr"<<std::endl;
            sameOrient=false;
        }
//        std::cout<<"fin tour du boucle des forces avec force = "<<force<<" resistance "<<resistance<<" i "<<i<<" j "<<j<<std::endl;
    }
//    std::cout<<std::endl;
//    std::cout<<"Fin du boucle des forces avec force = "<<force<<" resistance "<<resistance<<" i "<<i<<" j "<<j<<std::endl;
//    std::cout<<std::endl;
    ///Boucle de calcul des resistances
    while(m_plateau[i][j]!=nullptr && isInTab)
    {
//        std::cout<<"debut tour du boucle des resistances avec force = "<<force<<" resistance "<<resistance<<" i "<<i<<" j "<<j<<std::endl;
        char symb = m_plateau[i][j]->getSymbole();
        if(symb=='M') resistance+=0.9;
        if(symb=='E' || symb=='R')
        {
            if(m_plateau[i][j]->getOrientation()==orientation_opp) resistance+=1;
        }
        calcLigColAvecOrientation(j,i,orientation);
        isInTab=calcisInTab(j,i);
//        std::cout<<"fin tour du boucle des resistances avec force = "<<force<<" resistance "<<resistance<<" i "<<i<<" j "<<j<<std::endl;
    }

//    std::cout<<std::endl;
////    std::cout<<"Fin du boucle des resistances avec force = "<<force<<" resistance "<<resistance<<" i "<<i<<" j "<<j<<std::endl;
//    std::cout<<std::endl;
    calcLigColAvecOrientation(j,i,orientation_opp);
//    std::cout<<"je replace le curseur en arriere i="<<i<<" j="<<j<<std::endl;
//    std::cout<<"itemp ="<<itemp<<std::endl;
//    std::cout<<"jtemp ="<<jtemp<<std::endl;
////    calcLigColAvecOrientation(j,i,orientation_opp);
    jtemp=j;
    itemp=i;
//    std::cout<<"itemp ="<<itemp<<std::endl;
//    std::cout<<"jtemp ="<<jtemp<<std::endl;
//    std::cout<<std::endl;
    ///Compare Force et Resistance
    if(force>resistance)
    {
        bool surCaseDeDepart=false;
//         std::cout<<"Entree dans le if f>r avec force = "<<force<<" resistance "<<resistance<<" i "<<i<<" j "<<j<<"x0="<<x0<<"y0="<<y0<<std::endl;
        //deplace en chaine
        while(!(surCaseDeDepart))
        {

//            std::cout<<std::endl;
//            std::cout<<"Debut de tour du while de deplacement force = "<<force<<" resistance "<<resistance<<" i "<<i<<" j "<<j<<std::endl;
            itemp=i;
            jtemp=j;
            if(x0==jtemp && y0==itemp) surCaseDeDepart=true;
//            std::cout<<"itemp ="<<itemp<<std::endl;
//            std::cout<<"jtemp ="<<jtemp<<std::endl;
            calcLigColAvecOrientation(jtemp,itemp,orientation);
//            std::cout<<"apres recalcul des temps"<<std::endl;
//            std::cout<<"itemp ="<<itemp<<" i"<<i<<std::endl;
//            std::cout<<"jtemp ="<<jtemp<<" _j"<<j<<std::endl;
            //itemp et jtemp sont les coords increment� vers l'avant
            //case de l'avant prend la valeur de la case actuel
//            std::cout<<"m_plateau[itemp][jtemp]="<<std::endl;
            if(calcisInTab(jtemp,itemp))    m_plateau[itemp][jtemp]=m_plateau[i][j];
//            std::cout<<"m_plateau[itemp][jtemp]=m_plateau[i][j] FAIT"<<std::endl;
            //le cas o� une piece se fait sortir
            if(!calcisInTab(jtemp,itemp))
            {
//                std::cout<<"Entree du if(!calcisInTab(itemp,jtemp) force = "<<force<<" resistance "<<resistance<<" i "<<i<<" j "<<j<<std::endl;
                calcLigColAvecOrientation(jtemp,itemp,orientation_opp);
                Entite* pieceSortie =m_plateau[itemp][jtemp];
                m_plateau[itemp][jtemp]=nullptr;
                if(pieceSortie->getSymbole()=='E')
                    {
                        m_dehors_elephant.push_back(pieceSortie);
                        sound(son,1);
                    }
                if(pieceSortie->getSymbole()=='R')
                    {
                        m_dehors_rhino.push_back(pieceSortie);
                        sound(son,1);
                    }
                //Si une montagne est sortie
                if(pieceSortie->getSymbole()=='M')
                {
                    //je regarde les cases derieres
                    //je cherche l'animal le plus proche
//                    std::cout<<"if piecesortie est M f itemp="<<itemp<<" jtemp="<<jtemp<<std::endl;
                    int iwin=itemp;
                    int jwin=jtemp;
                    calcLigColAvecOrientation(jwin,iwin,orientation_opp);
//                    std::cout<<"avant while  iwin="<<iwin<<" jwin="<<jwin<<" symbole[iwin][jwin]="<<m_plateau[iwin][jwin]->getSymbole()<<std::endl;
                    while(!(m_plateau[iwin][jwin]->getSymbole()=='E' || m_plateau[iwin][jwin]->getSymbole()=='R' ))
                    {
//                         std::cout<<"while :  itemp="<<iwin<<" jtemp="<<jwin<<" symbole[iwin][jwin]="<<m_plateau[iwin][jwin]->getSymbole()<<std::endl;
                        calcLigColAvecOrientation(jwin,iwin,orientation_opp);
//                         std::cout<<"while : apres recalc jtemp et itemp="<<iwin<<" jtemp="<<jwin<<" symbole[iwin][jwin]="<<m_plateau[iwin][jwin]->getSymbole()<<std::endl;
                    }
                    //je designe le vainqueur
//                     std::cout<<"apres icalcul de la pos du vainqueur iwin="<<iwin<<" jwin="<<jwin<<std::endl;
                    if(m_plateau[iwin][jwin]->getSymbole()==p1.getPiece().at(0)) won(p1);
                    if(m_plateau[iwin][jwin]->getSymbole()==p2.getPiece().at(0)) won(p2);
                }

            }
            //je vais dans la case de derriere
            if(!(x0==j && y0==i)) calcLigColAvecOrientation(j,i,orientation_opp);
//            std::cout<<"fin de tour du while de deplacement force = "<<force<<" resistance "<<resistance<<" i "<<i<<" j "<<j<<std::endl;
//            std::cout<<std::endl;
        }
//        std::cout<<std::endl;
//        std::cout<<"Fin while de deplacement force = "<<force<<" resistance "<<resistance<<" i "<<i<<" j "<<j<<std::endl;
        if(x0==j && y0==i) m_plateau[i][j]=nullptr;
    }
    else{
        std::cout<<"Vous n'avez pas une force(="<<force<<") assez elevee pour pousser. (resistance ="<<resistance<<")"<<std::endl;
        std::cout<<"Vous venez donc de gacher un tour :)"<<std::endl;
        system("pause");
    }

    std::cout<<"Fin de la poussee"<<std::endl;
//   system("pause");

}
int Terrain::countPlateau(char& c)
{
    int counterM = 0;
    int counterE = 0;
    int counterR = 0;

    for (auto row : m_plateau)
    {
        for (auto elem : row)
        {
            if( elem->getSymbole() == 'M') counterM++;
            else if( elem->getSymbole() == 'R') counterR++;
            else if( elem->getSymbole() == 'E') counterE++;
        }
    }

    if( c == 'R') return counterR;
    else if (c == 'E') return counterE;
    else if (c == 'M') return counterM;
    else{
        exit(11);
        return 0;
    }

}
///Getters
vector<Entite*> Terrain::getDehorsRhino() const
{
    return m_dehors_rhino;
}
vector<Entite*> Terrain::getDehorsElephant() const
{
    return m_dehors_elephant;
}


