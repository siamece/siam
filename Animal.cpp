#include "Animal.h"
#include "console.h"
#include "Entite.h"
#include "fonctions.h"
#include "main.h"
#include "Montagne.h"
#include "player.h"
#include "Terrain.h"

using namespace std;

Animal::Animal()
{
    //ctor
}

Animal::Animal(string _nom)
{
     m_nom=_nom;

    m_poids = 1.0;
    if (_nom == "Elephant") m_symbole = 'E';
    else if (_nom == "Rhinoceros") m_symbole = 'R';
    else m_symbole = 'p'; //probl�me si ca s'affiche
}
Animal::Animal(char symbole,string nom)
{
    m_nom=nom;
    m_symbole=symbole;

    m_poids=1.0;
}

Animal::~Animal()
{
    //dtor
}

string Animal::getNom() const
{
    return m_nom;
}

void Animal::setNom(string _nom)
{
    m_nom = _nom;
}


