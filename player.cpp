#include "Animal.h"
#include "console.h"
#include "Entite.h"
#include "fonctions.h"
#include "main.h"
#include "Montagne.h"
#include "player.h"
#include "Terrain.h"

using namespace std;

Player::Player()
    : m_name(""), m_score(0)
{
    m_hasWon=false;
}

Player::Player(string _piece)
    : m_piece(_piece)
{
    m_hasWon=false;
}

Player::~Player()
{

}

string Player::getName() const
{
    return m_name;
}

string Player::getPiece() const
{
    return m_piece;
}

int Player::getScore() const
{
    return m_score;
}

bool Player::getHasPlayed() const
{
    return m_hasPlayed;
}
bool Player::getHasWon() const
{
    return m_hasWon;
}
void Player::setName(string name)
{
    m_name = name;
}

void Player::setScore(int score)
{
    m_score = score;
}

void Player::setHasPlayed(bool play)
{
    m_hasPlayed = play;
}

void Player::setHasWon(bool haswon)
{
    m_hasWon=haswon;
}
