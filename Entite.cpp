#include "Animal.h"
#include "console.h"
#include "Entite.h"
#include "fonctions.h"
#include "main.h"
#include "Montagne.h"
#include "player.h"
#include "Terrain.h"

using namespace std;

Entite::Entite()
{
}
Entite::Entite(float poids)
        :m_poids(poids)
{

}
Entite::~Entite()
{
}


char Entite::getSymbole() const
{
    return m_symbole;
}
char Entite::getOrientation() const
{
    return m_orientation;
}
float Entite::getPoids() const
{
    return m_poids;
}

void Entite::setSymbole(char _symbole)
{
    //if((_symbole == '') || (_symbole == '') || (_symbole == '') || (_symbole == ''))
    //{
        m_symbole = _symbole;
    //}
}
void Entite::setOrientation(char _orientation)
{
    if( (_orientation == 'h') || (_orientation == 'b') || (_orientation == 'g') || (_orientation == 'd') )
    {
        m_orientation = _orientation;
    }
}
void Entite::setPoids(float _poids)
{
    if(_poids >= 0)
    {
        m_poids = _poids;
    }
}
