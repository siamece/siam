#ifndef TERRAIN_H
#define TERRAIN_H
#include "main.h"
#include "Entite.h"
#include "console.h"
#include "player.h"

class Terrain
{
    public:
        Terrain();
        virtual ~Terrain();

        //Getters
        bool getIsOver() const;
        bool getGraphMode() const;
        std::vector<std::vector<Entite*>> getPlateau() const;
        std::vector<Entite*> getDehorsRhino() const;
        std::vector<Entite*> getDehorsElephant() const;


        //Setters
        void setPlateau(std::vector<std::vector<Entite*>> plateau);
        void setIsOVer(bool b);
        void setGraphMode(bool b);

        //functions
        void initialisationPlateau();
        int countPlateau(char& c);
        void afficher(int pos_x, int pos_y,Console* pConsole);
        void recommencer();
        void won(Player& p);
        void faire_entrer_animal(char symbole,char orientation,unsigned int x,unsigned int y,Player& p1, Player& p2, SAMPLE* son[BMP_NB]); //entre un animal dans le plateau aux coordonnées
        void faire_sortir_animal(const Player& j,unsigned int x,unsigned int y);
        void deplacer_sur_case_vide(int x0,int y0,int x1,int y1,char orientation);
        void deplacer_en_poussant(int x0,int y0,char orientation,Player &p1,Player & p2,SAMPLE* son[SON_NB],bool isEntrying=false);

    protected:

    private:

        bool m_isOver = false;
        std::vector<Entite*> m_dehors_rhino;
        std::vector<Entite*> m_dehors_elephant;
        std::vector<std::vector<Entite*>> m_plateau;
        //Entite* m_plateau_2[5][5];
        bool m_graph_mode = false;

        void calcLigColAvecOrientation(int & x,int & y,char orientation);
        char calcOrientationOpposee(char &orientation);
        bool calcisInTab(int x,int y);



};

#endif // TERRAIN_H
