#include "main.h"


bool caseEstAdjacente(int x0,int y0,int x1,int y1);
void ajoutPion(Player& p, Terrain& terrain, Console* pConsole,Player& p2,SAMPLE* son[SON_NB]);
void sortiePion(Player& p, Terrain& terrain, Console* pConsole);
void deplacerPion(Player& p, Terrain & terrain, Console* pConsole,Player& p2,SAMPLE* son[SON_NB]);
void orientationPiece(Player& p, Terrain& terrain, Console* pConsole);
bool caseInput(std::string& data);
void caseTreatement(std::string& data, int& x, int& y);
bool choixOrientation(char& orient);
